"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Course: Big Data in Finance (Spring 2020)
Date: 2020-02
Code:
    Homework II solutions. Portfolio sorts.

------

Dependence:
fire_pytools

"""

# %% Packages
import pandas as pd
import matplotlib.pyplot as plt

import homeworks.hm_ii.solutions.data_import.stock_annual as stock_annual
import homeworks.hm_ii.solutions.data_import.stock_monthly as stock_monthly

# Packages from fire_pytools
from utils.monthly_date import *

from portools.find_breakpoints import find_breakpoints
from portools.sort_portfolios import sort_portfolios

desired_width = 10
pd.set_option('display.width', desired_width)
idx = pd.IndexSlice

# %% Set Up

# %% Download Data
# Monthly Data
mdata = stock_monthly.main()

# Annual Data
adata = stock_annual.main()

# Set names
adata.drop(columns='inv', inplace=True)
adata.rename(columns={'mesum_june': 'me',
                      'inv_gvkey': 'inv'},
             inplace=True) #inv_permco

# %% Create Filters
# shrcd must be (10,11)
# ---------------------
print('Data deleted due to shrcd: %f' % np.round((1-adata.shrcd.isin([10, 11]).mean())*100, 2))
sort_data = adata[adata.shrcd.isin([10, 11])].copy()
mdata = mdata[mdata.shrcd.isin([10, 11])]

# exchcd must be (1, 2, 3)
# ------------------------
print('Data deleted due to exchcd: %f' % np.round((1-sort_data.exchcd.isin([1, 2, 3]).mean())*100, 2))
sort_data = sort_data[sort_data.exchcd.isin([1, 2, 3])]
mdata = mdata[mdata.exchcd.isin([1, 2, 3])]

#del adata, mdata

# %% Analyses of returns
print("Descriptive statistics for ret_11_1")
data_dec2019 = mdata[mdata.date == '2019-12-31'].copy()

print("1. Number of missings.")
print(data_dec2019.ret_11_1.isnull().sum())

print("2. Number of unique permnos")
data_dec2019.permno.nunique()

print("3. Descriptive statistics")
print(data_dec2019.ret_11_1.describe())

print('Top 10 returns as December 2019')
data_dec2019.sort_values('ret_11', inplace=True, ascending=False)
print(data_dec2019[['comnam', 'ticker', 'ret_11', 'ret_11_1']][1:10])

print('Bottom 10 returns as December 2019')
data_dec2019.sort_values('ret_11', inplace=True, ascending=True)
print(data_dec2019[['comnam', 'ticker', 'ret_11', 'ret_11_1']][1:10])

# %% Portfolio Sorts
 # Breakpoints
char_breakpoints = {'me': [0.5],
                    'beme': [0.3, 0.7],
                    'opbe': [0.3, 0.7],
                    'inv': [0.3, 0.7]}
sortvars = [i for i in char_breakpoints.keys()]

# Define the sample filter
# ------------------------
sample_filters = [None] * len(sortvars)
sample_filters = dict(zip(sortvars, sample_filters))

for sortvar in sortvars:
    # notice that the way we defined beme is null if be<=0
    sample_filters[sortvar] = ((sort_data.me > 0) &
                               (sort_data.mesum_dec > 0) &
                               (sort_data[sortvar].notnull()))

portsorts = [None] * len(sortvars)
portsorts = dict(zip(sortvars, sample_filters))

for sortvar in sortvars:
    portsorts[sortvar] = sort_portfolios(data=sort_data[sample_filters[sortvar]],
                                         quantiles={sortvar: char_breakpoints[sortvar]},
                                         id_variables=['rankyear', 'permno', 'exchcd'],
                                         exch_cd=[1]
                                         )

# merge all separate portfolio allocations together
port = portsorts['me'].copy()
for sortvar in sortvars[1:]:
    # Notice that this is an outer join
    port = port.merge(portsorts[sortvar],
                      on=['permno', 'rankyear'],
                      how='outer')

# Calculate the value spread
char_data = pd.merge(port,
                     sort_data[['permno', 'rankyear', 'be', 'mesum_dec', 'inv', 'op', 'at']])

beme_char = char_data[['rankyear', 'meportfolio', 'bemeportfolio', 'be', 'mesum_dec']].groupby(['rankyear', 'meportfolio', 'bemeportfolio']).sum()
beme_char['beme'] = beme_char['be']/beme_char['mesum_dec']
beme_char = beme_char['beme'].unstack(level=[1, 2])

value_spread = np.log(beme_char.loc[:, idx[:, 'beme3']].mean(axis=1)) - np.log(beme_char.loc[:, idx[:, 'beme1']].mean(axis=1))
value_spread.plot(title='The Value Spread', grid=True)
plt.show()

# Calculate the profitability spread
opbe_char = char_data[['rankyear', 'meportfolio', 'opbeportfolio', 'op', 'be']].groupby(['rankyear', 'meportfolio', 'opbeportfolio']).sum()
opbe_char['opbe'] = opbe_char['op']/opbe_char['be']
opbe_char = opbe_char['opbe'].unstack(level=[1, 2])

prof_spread = opbe_char.loc[:, idx[:, 'opbe3']].mean(axis=1) - opbe_char.loc[:, idx[:, 'opbe1']].mean(axis=1)
prof_spread.plot(title='The Profitability Spread', grid=True)
plt.show()

prof_spread[1990:].plot(title='The Profitability Spread', grid=True)
plt.show()

# Calculate the investment spread
inv_char = char_data[['rankyear', 'meportfolio', 'invportfolio', 'at']].groupby(['rankyear', 'meportfolio', 'invportfolio']).sum()
inv_char = inv_char.unstack(level=[1, 2])
inv_char = (inv_char/inv_char.shift(1))-1

inv_spread = inv_char.loc[:, idx['at', :, 'inv3']].mean(axis=1) - inv_char.loc[:, idx['at', :, 'inv1']].mean(axis=1)
inv_spread.plot(title='The Investment Spread', grid=True)
plt.show()








